"""M345SC Homework 1, part 2
Name: Calum Dick-Oakley CID:01071441
"""

def rng_search(L,x):

    istart = 0
    iend = len(L)-1
    fst_x = -1 # Will store the first time we find an occurence of x

    # First we use the regular binary search to find the first occurence

    # I have slightly altered the algorithm given in labs by first checking if
    # x is in the range. I have put this in since we dont know the type of data
    # we will be searching and for just a constant cost we have the chance to
    # skip out on this whole search.


    if L[0]<= x and L[iend]>= x:
        while istart<=iend:

            imid = int(0.5*(istart+iend))

            if x==L[imid]:
                fst_x = imid
                break
            elif x < L[imid]:
                iend = imid-1
            else:
                istart = imid+1

        if fst_x == -1: #Return if we cant find any x
            return -1,-1

        # At this point I have found a place where x appears. At this point we
        # also know that iend and istart are upper and lower bounds for the last
        # and first appearence of x.

        #Now I find the first occurence:

        if L[istart] == x:#If this is the case then istart is already the first occurence
            lo_x = istart
        else:
            hi = fst_x #the upper bound for the first occurence of x
            while istart + 1 < hi:
                imid = int(0.5*(istart+hi))
                if L[imid] == x:
                    hi = imid
                else:
                    istart = imid
            lo_x =  hi

        #Now we do the same for the last occurence

        if L[iend] == x:
            hi_x = iend
        else:
            lo = fst_x
            while lo + 1 < iend:
                imid = int(0.5*(iend+lo))
                if L[imid] == x:
                    lo = imid
                else:
                    iend = imid
            hi_x =  lo

        return lo_x,hi_x

    return -1,-1

def nsearch(L,P,target):
    """Input:
    L: list containing *N* sub-lists of length M. Each sub-list
    contains M numbers (floats or ints), and the first P elements
    of each sub-list can be assumed to have been sorted in
    ascending order (assume that P<M). L[i][:p] contains the sorted elements in
    the i+1th sub-list of L
    P: The first P elements in each sub-list of L are assumed
    to be sorted in ascending order
    target: the number to be searched for in L

    Output:
    Lout: A list consisting of Q 2-element sub-lists where Q is the number of
    times target occurs in L. Each sub-list should contain 1) the index of
    the sublist of L where target was found and 2) the index within the sublist
    where the target was found. So, Lout = [[0,5],[0,6],[1,3]] indicates
    that the target can be found at L[0][5],L[0][6],L[1][3]. If target
    is not found in L, simply return an empty list (as in the code below)
    """

    Lout=[]

    for n in range(len(L)):#We iterate over each subarray in L

        [ord_fst,ord_lst] = rng_search(L[n][:P], target) #We use our binary search on the orderd part

        if ord_fst != -1:#If some were found then we add them
            for i in range(ord_fst,ord_lst+1):
                Lout += [[n,i]]

        for i, k in enumerate(L[n][P:],P): #Normal linear search on the unordered part of L
            if k == target:
                Lout += [[n,i]]


    return Lout


def nsearch_time():
    """Analyze the running time of nsearch.
    Add input/output as needed, add a call to this function below to generate
    the figures you are submitting with your codes.

    Discussion: I will discuss a) and b) of question 2 simultaneously:

    The first part of the algorithm that I will discuss the is rng_search function
    that finds the range of positions for which a value appears in a sorted list.
    This function starts by using the regular binary search to find the first
    occurence of x in the list. I will not talk about this as it has been dicussed
    in class, this has running time O(log_2(N)) where N is the length of the list.
    Note also that the initial check whether or not x is in the range of the list
    is a constant time search and as such does not effect the order of this
    funciton.

    The next two parts use the output from the regular binary search to find the
    first and last occurence. Much like the normal binary search we can half the
    range we are searching over after each iteration. As such this has a running
    time O(log_2(n)) where n is 0.5*(iend-istart), since this range is clearly
    bounded above by N we have thatfinding the first and runs at most O(log_2(N)).
    Since we one part after another we can just add their running times to see
    that the rng_search function has running time O(log_2(N)).

    Now we understand how rng_search works and its running time we can understand
    the running for nsearch as a whole.

    The first part is a loop over the N sublists of the input list, because of
    this we only need to consider how the function behaves when given a list
    with just one sublist, then we can multiply this behaviour by N. Given a
    list containing one list nsearch has two main parts.

    Firstly it calls upon rng_search, as we defined above, on the first P
    elements of the sublist. This runs in O(log_2(P)) time and gives us the
    positions that x appears in the sorted part of the list. We then have to add
    these positions to Lout, for this we have as many operations as there are
    occurences of x, this obviously depends on the type of data being collected
    but at a worst case this is P.

    Secondly we perform a linear search on the remaining M-P elements of the list.
    As discussed in lectures this linear search over M-P elements will require
    3(M-P) operations and hence this part is O(M-P).

    We can combine all of this together to find that the total running time of
    the algorithm is O(N(log_2(P)-P+M)). Note that in this running time I have
    made the assumption that the sorted part of the list contains less than
    log_2(P) occurences of x and so log_2(P) is the largest contributer to the
    order from the sorted part. As a worst case estimate we would have that all
    elements in the sorted list are x, hence we would have P more operations
    when adding to Lout, this gives a worst case estimate of O(N(log_2(P)+M)).

    Finally a best case scenario is when x does not occur in the range of the
    ordered list, in this case it is a constant time search on the ordered part.
    Hence the best case estimate is O(N(M-P)).

    c) I believe that my algorithm can be considered efficient. There are three
    reasons for this.

    Firstly, since the sublists of the input L are not necessarily related in any
    way we have no way to check them other than doing each one separately. This
    gives an overall factor of N in our efficiency which cannot be avoided.

    Secondly, in each sublist the final M-P elements have no structure and so at
    the very least we must check each element to see if it is equal to x. Thus
    we cannot beat a linear search, which is what we have done.

    Finally, we know from lectures that the fastest way to search an ordered
    list is in log_2(P) time. Now since we have used another binary search
    (rather than a linear one) to extend one occuence to all of them we still
    have an order of log_2(P).

    In conclusion, each stage of my algorithm runs in a minimal possible order.
    It may be the case that there are places to make savings however these savings
    will not alter the order that the algorithm runs in.

    d) and e) Fig1: This plot shows what happens when we fix M and P and vary N.
    As we can see there is a linear relationship as expected.

    Fig2: Now we know how N effects the order we can set it to 1 to speed up
    our remaining tests. This figure shows what happens when we have N=1 and
    have P = 0.95M but we increase M on a log scale. We do this because when M
    is small we have that log_2(0.95M) is larger than 0.05M and so the order is
    dominated by the log_2(P) term, hence on a log scale the graph is linear.
    However as M gets larger eventually the linear term dominates the log term,
    thus on a log scale the graph should become exponential.

    We can actually calculate specifics here, in lectures we had that a binary
    search has a order of log_2(P), since in each loop we do three operations we
    can say that the binary search takes 3log_2(P) operations. Since we also do
    this for min and max we take 9log_2(P) operations in total. The linear part
    of this function then takes 3(M-P) operations, again this was given in
    lectures. So we can now actually calculate when these are equal, in this
    case we are looking for when 9log_2(0.95M) = 3(0.05M). We can solve this
    numerically to find that the intersection is at M = 540. On our log based
    scale this corresponds to 5.75 as 10*2^(5.75) = 540. This is exactly what we
    see on our graph, up to 6 the graph is linear and then from 6 onward the
    graph becomes exponential as expected.

    Fig3: Here we fix an N and see what happens when we increase P. From our
    calcualtions, since M is fixed we would expect the plot of time to behave
    like log(P)-P. This is what we see in Fig3, the initial increase followed
    immediatly after by an almost linear decrease.


    """



    import numpy as np
    import time
    import matplotlib.pyplot as plt

    # Here I create a function that will make random list in order to test our
    # fucntion with
    def lst_gen(N,M,P):

        lst = []

        for n in range(N):
            ordered = list(np.sort(np.random.randint(1,100,P)))
            unordered = list(np.random.randint(1,100,M-P))
            lst += [ordered + unordered]

            return lst

    def nsearch_timer_N(): # varying N while fixing P and M

        times = []

        for n in range(1,200,5):
            tim = []
            for k in range(10): #loop 10 times to average
                lst = lst_gen(n,1000,800) #Create a random list
                targ = np.random.randint(1,100) #Random integer to look for
                t1 = time.time()
                srch = nsearch(lst, 800, targ)
                t2 = time.time()
                tim += [t2-t1]

            times += [np.average(tim)] #Average the ten times recorded

        plt.plot(list(range(1,200,5)), times, 'ro')
        plt.title('Calum Dick-Oakley nsearch_timer_N()')
        plt.xlabel('N')
        plt.ylabel('Time')
        plt.show()


    def nsearch_timer_rat(): # varying P while fixing N and M, follows log(x)-x as expected

        times = []

        M = 10

        for n in range(12):
            tim = []

            for k in range(1000):#loop to average
                lst = lst_gen(1,M,int(0.95*M))
                targ = np.random.randint(1,100)
                t1 = time.time()
                srch = nsearch(lst, int(0.95*M), targ)
                t2 = time.time()
                tim += [t2-t1]

            M = 2*M
            times += [np.min(tim)]

        plt.plot(list(range(1,13)), times, 'ro')
        plt.title('Calum Dick-Oakley nsearch_timer_rat()')
        plt.xlabel('M')
        plt.ylabel('Time')
        plt.show()

    def nsearch_timer_P(): # varying P while fixing N and M, follows log(x)-x as expected

        times = []

        for n in range(1,100,1):
            tim = []
            for k in range(100):#loop 4 times to average
                lst = lst_gen(1,100,n)
                targ = np.random.randint(1,100)
                t1 = time.time()
                srch = nsearch(lst, n, targ)
                t2 = time.time()
                tim += [t2-t1]

            times += [np.average(tim)]

        plt.plot(list(range(1,100,1)), times, 'ro')
        plt.title('Calum Dick-Oakley nsearch_timer_P()')
        plt.xlabel('P')
        plt.ylabel('Time')
        plt.show()

    return times

    return None #Modify as needed


if __name__ == '__main__':



    #add call(s) to nsearch here which generate the figure(s)
    #you are submitting
    nsearch_time() #modify as needed
    nsearch_timer_N()
    nsearch_timer_rat()
    nsearch_timer_P()
