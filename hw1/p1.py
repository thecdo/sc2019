"""M345SC Homework 1, part 1
Name: Calum Dick-Oakley CID: 01071441
"""

def ksearch(S,k,f,x):
    """
    Search for frequently-occurring k-mers within a DNA sequence
    and find the number of point-x mutations for each frequently
    occurring k-mer.
    Input:
    S: A string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which
    occur at least f times in S
    x: the location within each frequently-occurring k-mer where
    point-x mutations will differ.

    Output:
    L1: list containing the strings corresponding to the frequently-occurring k-mers
    L2: list containing the locations of the frequent k-mers
    L3: list containing the number of point-x mutations for each k-mer in L1.

    Discussion: I will dicuss both parts a) and b) simultaneously, giving a
    basic outline while also giving its cost.

    My approach to this question was to create a dictionary storing
    all strings that occurred in the DNA sequence. We can then create our L1,L2
    and L3 by pulling the correct data from this dictionary.

    My first step is to create a smaller dictionary that I call let_dic, this
    allows us to easily convert between letters and their associated numbers.
    This will help us in the creation of L3.

    The next step is to create the actual dictionary, in order to do this we
    loop over all possible kmers starting from the first k-substring and ending
    in the last. The information is stored in the dictionary by associating the
    kmer string, as the key, with a list of all the times that the kmer occurs.

    I claim that this stage of the algorithm will take O(k(n-k)) time where n is
    the length of the DNA sequence. This is because the act of adding an element
    to a dictionary or checking if an element is in a dictionary are both order
    1. Hence the total order of this step is the length of the for loop times
    the cost of extracting the k-mer, this is the cost of slicing which is is O(k).

    All of this put together means that the leading order running time is k(n-k),
    this is because as we increse the value of k the extra two operations in the
    for loop are outweighed by the cost of extracting the kmer.

    In the next stage we run through our dictionary and pick out all k-mers that
    satisfy our conditions. We don't actually know how many of these there
    however we do have an upper bound, namely the total number of substrings we
    looped over, n-k+1. Similarly we can create an upper bound for the number of
    kmers that occur more than f times, namely (n-k+1)/f, since that is the
    amount we have when as many kmers as possible occur f times.

    Now in this case there are two operations, one to get the key in L1 and the
    other to put the positions in L2. Now in order to create L3 we have to loop
    again over the three possible mutated kmers, this is roughly 30 operations.

    Since we are doing this at most (n-k+1)/f times we have that for f large
    this will be far smaller than (n-k+1) from the first part. Hence we can
    conclude that the total order of our algorithm is O(k(n-k)) with a leading
    order term of k(n-k).
    """

    L1,L2,L3=[],[],[]

    #Here I create a dictionary that allows us to convert between letters and an associated number for it.
    let_dic = {}
    let_dic['A'] = 0
    let_dic['C'] = 1
    let_dic['G'] = 2
    let_dic['T'] = 3
    let_dic[0] = 'A'
    let_dic[1] = 'C'
    let_dic[2] = 'G'
    let_dic[3] = 'T'

    dic = {} #We will create a dictionary that stores all the kmers

    dic[S[0:k]] = [0] # For the first kmer we store [1,0], indicating that so far it appears once a position 0

    for n in range(1,len(S) - k + 1): #Now we can iterate over all kmers, adding as we go along
        kmer = S[n:n+k] #The kmer we are testing
        if kmer in dic: #If the string has already occured
            dic[kmer] += [n] #Then we add this new position to the entry
        else:
            dic[kmer] = [n] #If the string has not occured yet we add it to the dictionary.

    for key in dic:
        if len(dic[key]) >= f: #Now we look for all strings that occur more than f times
            L1 += [key] #Then we add this key to L1
            L2 += [dic[key]] #And add the positions to L2
            L3 += [0] #Add the running count for the mutated kmers

            letter = let_dic[key[x]] #This is the code of letter that we will be finding a mutation in

            for n in range(1,4):
                new_let = let_dic[(letter + n) % 4] #We add one each time and take mod 4 to find the new letter
                new_key = key[:x] + new_let + key[x+1:] #We use this to create the mutated kmer

                if new_key in dic: #We check if this mutated kmer appears
                    L3[-1] += len(dic[new_key]) #If it does we add its count to the total

    return L1,L2,L3




if __name__=='__main__':
    #Sample input and function call. Use/modify if/as needed
    S='CCCTATGTTTGTGTGCACGGACACGTGCTAAAGCCAAATATTTTGCTAGT'
    #k=3
    #x=2
    #f=2
    #L1,L2,L3=ksearch(S,k,f,x):
